﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace listviewhomework
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class details : ContentPage
    {
        public details()
        {
            InitializeComponent();
        }
        private async void First_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1()); // this method opens page 1 when it is executed
        }

        private async void Second_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2()); //opens page 2
        }
    }
}