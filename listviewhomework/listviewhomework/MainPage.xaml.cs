﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace listviewhomework
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
            pageView.ItemsSource = new List<mainpageview> 
            {
                new mainpageview
                {
                    Image = "APPLE.jpeg",
                    Name = "Apple",
                    Color = "Red",
                    Vitamins ="A,B,C,Fiber",
                    

                },
                new mainpageview
                {
                    Image = "banana.jpeg",
                    Name = "Banana",
                    Color = "Yellow",
                    Vitamins ="A,K" ,
                    
                },
                new mainpageview
                {
                    Image = "berries.jpeg",
                    Name = "berries",
                    Color = "multicolor",
                    Vitamins ="C,D,Antioxidants"
                },
                new mainpageview
                {
                    Image = "cantaloupe.jpg",
                    Name = "melon",
                    Color = "greenish",
                    Vitamins ="C,E"
                },
                new mainpageview
                {
                    Image = "cherry.jpg",
                    Name = "Cherries",
                    Color = "Bright Red",
                    Vitamins ="Antioxidants"
                },
                new mainpageview
                {
                    Image = "grapes.jpg",
                    Name = "Grapes",
                    Color = "Red",
                    Vitamins ="antioxidants"
                },
                new mainpageview
                {
                   Image = "orange.jpeg",
                    Name = "Oranges",
                    Color = "Orange",
                    Vitamins ="C,A,antooxidants"
                },
                new mainpageview
                {
                    Image = "pineapple.jpg",
                    Name = "pineapple",
                    Color = "greenish",
                    Vitamins = "C,Antioxidants"
                },
                new mainpageview
                {
                    Image = "strawberry.jpeg",
                    Name = "Strawberry",
                    Color = "Red",
                    Vitamins ="C,D"
                },
                new mainpageview
                {
                    Image = "watermelon.jpeg",
                    Name = "watermelon",
                    Color = "red",
                    Vitamins ="K,fibre"
                }
            };
        }

        public MainPage(string name, string n, string e )
        {
            InitializeComponent();
        }

        void H_Refreshing(object sender, System.EventArgs e)
        {
           
            pageView.IsRefreshing = false;
        }
       
        async void action(object sender, SelectedItemChangedEventArgs e) // tapped event 
        {
             
            var listview = (ListView)sender;
            mainpageview litem = (mainpageview)listview.SelectedItem; 
            await Navigation.PushAsync(new aboutpage(litem)); // when cell is tapped aboutpage is displayed
            
            
            

        }
        
        async void ToModify(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new menupage()); //when you press long hold on any cell menu page will appaer
        }


    }
}
