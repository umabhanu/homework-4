﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace listviewhomework
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class aboutpage : ContentPage
    {
        public aboutpage()
        {
            InitializeComponent();

        }
        public aboutpage(mainpageview vv)
        {
            InitializeComponent();
            var ll = new List<mainpageview> {
            new mainpageview
            {
                Name1 = "about fruits",
                Image="Fruit.jpg"
                
            }
            };

        }
        async void whenclicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync(); //takes back to root page
        }
        async void onclicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NavigationPage(new details())); //takes to navigation page
        }

    }
}